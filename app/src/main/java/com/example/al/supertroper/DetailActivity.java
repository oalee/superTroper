package com.example.al.supertroper;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


public class DetailActivity extends Activity {
    private String STORY_OBJECT_KEY = "story_object_key";
    private String TAG = "DetailActivity";
    private boolean debug = true;
    private boolean isColorChanged = false;
    private boolean isFullScreen = false;

    VideoView mVideoView;
    StoryObject storyObject;
    FrameLayout container;
    RelativeLayout relativeLayoutContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_detail);
        storyObject = new StoryObject(getIntent().getStringExtra(STORY_OBJECT_KEY));

        if (debug)
            Log.d(TAG, "get storyObject " + storyObject.toString());


        relativeLayoutContainer = (RelativeLayout) findViewById(R.id.activity_detail_relative_layout_container);

        container = (FrameLayout) findViewById(R.id.activity_main_container);

        FrameLayout scrollViewContainer = (FrameLayout) findViewById(R.id.activity_detail_scrollView_container);

        FrameLayout.LayoutParams scrollViewContainerParams = new FrameLayout.LayoutParams((int) (DeviceDimention.getScreenWidth() * 0.8)
                , (int) (DeviceDimention.getScreenHeight() * 0.43));
        scrollViewContainerParams.topMargin = (int) (DeviceDimention.getScreenHeight() * 0.43);
        scrollViewContainerParams.gravity = Gravity.CENTER_HORIZONTAL;

        scrollViewContainer.setLayoutParams(scrollViewContainerParams);


        FrameLayout scrollView = (FrameLayout) findViewById(R.id.activity_detail_scrollView);
        FrameLayout.LayoutParams scrollBarParams = new FrameLayout.LayoutParams((int) (DeviceDimention.getScreenWidth() * 0.75)
                , (int) (DeviceDimention.getScreenHeight() * 0.30));
        scrollBarParams.topMargin = (int) (DeviceDimention.getScreenHeight() * 0.008);
        scrollBarParams.gravity = Gravity.CENTER;
        scrollView.setLayoutParams(scrollBarParams);


        TextView mainDetail = (TextView) findViewById(R.id.acitivty_detail_main_text);
        mainDetail.setText(
                storyObject.fullDetail
        );
        mainDetail.setTypeface(Utilities.getFontLight());
        mainDetail.setGravity(Gravity.CENTER);
        mainDetail.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);


        FrameLayout titleContainer = new FrameLayout(this);
        titleContainer.setBackgroundColor(Color.WHITE);

        FrameLayout.LayoutParams titleContainerParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT
                , (int) (DeviceDimention.getScreenHeight() * 0.1));
        titleContainerParams.topMargin = (int) (DeviceDimention.getScreenHeight() * 0.38);
        titleContainerParams.gravity = Gravity.CENTER_HORIZONTAL;


        TextView titleTextView = new TextView(this);
        titleTextView.setText(storyObject.title);
        titleTextView.setTypeface(Utilities.getFontLight());

        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        titleTextView.setBackgroundResource(R.drawable.text_acitivity_detail_title_border_style);

        FrameLayout.LayoutParams titleTextViewParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, (int) (DeviceDimention.getScreenHeight() * 0.1));
        titleTextViewParams.gravity = Gravity.CENTER_VERTICAL;
        titleTextView.setGravity(Gravity.CENTER_VERTICAL);

        titleContainer.addView(titleTextView, titleTextViewParams);

        container.addView(titleContainer, titleContainerParams);

        final RelativeLayout pBarContainer = (RelativeLayout) findViewById(R.id.container_progress_bar_in_detail);
//        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_in_detail);
//
        SizeConverter pSizeConverter = SizeConverter.SizeConvertorFromWidth(DeviceDimention.getScreenWidth(), 640, 360);
        RelativeLayout.LayoutParams pBarParams = new RelativeLayout.LayoutParams(pSizeConverter.mWidth, pSizeConverter.mHeight);
//                ;
        pBarContainer.setLayoutParams(pBarParams);
//        pBarParams.addRule(RelativeLayout.CENTER_IN_PARENT);
//        progressBar.setLayoutParams();


        mVideoView = (VideoView) findViewById(R.id.video_view);
        String proxyUrl = (storyObject.mp4Url);
        mVideoView.setVideoPath(proxyUrl);
        final CustomMediaController mediaController = new CustomMediaController(this);
        mediaController.setListener(new CustomMediaController.OnMediaControllerInteractionListener() {
            @Override
            public void onRequestFullScreen() {
                if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                else
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });
        mVideoView.requestFocus();
        mVideoView.setBackgroundColor(Color.WHITE);

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {

                mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(MediaPlayer mp, int percent) {

                        if (!isColorChanged) {
                            mVideoView.setMediaController(mediaController);
                            mVideoView.setBackgroundColor(Color.TRANSPARENT);
                            mediaController.setAnchorView(mVideoView);
                            pBarContainer.setVisibility(View.GONE);
                            mVideoView.seekTo(0);
                            isColorChanged = true;
                        }

                        if (debug)
                            Log.d(TAG, "buffering update listener " + percent);
                    }
                });


                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        if (isColorChanged)
                            return;
                        mVideoView.setMediaController(mediaController);
                        mVideoView.setBackgroundColor(Color.TRANSPARENT);
                        mediaController.setAnchorView(mVideoView);
                        pBarContainer.setVisibility(View.GONE);

                        isColorChanged = true;
                    }
                });


                mVideoView.start();
            }
        });


//
//        Log.d("tag", "log works");

    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "pauesd");
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mVideoView.getLayoutParams();

            isFullScreen = true;
            params.setMargins(0, 0, 0, 0);
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            mVideoView.setLayoutParams(params);
            relativeLayoutContainer.setBackgroundColor(Color.BLACK);
            container.setVisibility(View.GONE);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            isFullScreen = false;
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 0);
            relativeLayoutContainer.setBackgroundColor(Color.WHITE);
            mVideoView.setLayoutParams(params);
            container.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (isFullScreen) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


}
