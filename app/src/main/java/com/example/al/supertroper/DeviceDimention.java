package com.example.al.supertroper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

public class DeviceDimention {

    private  static  String TAG ="DeviceDimention";
    private static boolean debug = true;
    private static int screenHeight = 0;
    private static int screenWidth = 0;

    public static int getScreenHeight() {
        return screenHeight;
    }


    public static int getScreenWidth() {
        return screenWidth;
    }


    public static void initSizes(Activity activity) {
        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                activity.getWindowManager().getDefaultDisplay().getSize(size);
                screenWidth = size.x;
                screenHeight = size.y;
            } catch (NoSuchMethodError e) {

                DisplayMetrics metrics = new DisplayMetrics();
                activity.getWindowManager().getDefaultDisplay()
                        .getRealMetrics(metrics);
                screenWidth = metrics.widthPixels;
                screenHeight = metrics.heightPixels;

            }

        }
        else {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = metrics.widthPixels;
            screenHeight = metrics.heightPixels;
        }



    }


}
