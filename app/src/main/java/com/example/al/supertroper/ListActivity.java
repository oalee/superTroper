package com.example.al.supertroper;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.android.volley.Response;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by al on 11/8/15.
 */


// TODO add more views than 5

public class ListActivity extends Activity {

    DragableLayout dragableLayout;
    VerticalViewPager viewPager;
    MyViewPagerAdapter myViewPagerAdapter;
    FrameLayout mustUpdateView;
    FrameLayout container;
    public boolean shallNotPass = false;
    String TAG = "ListActivity";
    private boolean debug = true;
    String name = "List News";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Utilities.setContext(getApplicationContext());


        Utilities.getAppVersion();

        Utilities.checkFromAPI(new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
//                doThingsToJson(response);
            }
        }, new Utilities.OnBasicInformationListener() {
            @Override
            public void onBasicInformation(int appVersion, int batch, boolean isNewBatch) {
                youShallOrNotPass();
            }
        });
        myViewPagerAdapter = new MyViewPagerAdapter(this);


        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        setContentView(R.layout.activity_list);
        DeviceDimention.initSizes(this);
        container = (FrameLayout) findViewById(R.id.activity_list_main_container);

        viewPager = new VerticalViewPager(this);
        viewPager.setAdapter(myViewPagerAdapter);
        container.addView(viewPager);


        dragableLayout = new DragableLayout(this);
        RelativeLayout dragableContainer = new RelativeLayout(this);
        container.addView(dragableContainer, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dragableContainer.addView(dragableLayout);
        viewPager.setOnPageChangeListener(new VerticalViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (shallNotPass)
                    return;

                int lastBatch = Utilities.getLastBatchNumber();
                int currentBatch = (lastBatch - (position / 5));

                if(debug)
                    Log.d(TAG , "last batch is " + lastBatch  + " current view batch is " + currentBatch);
                if (position % 5 == 0) {

                    setDragableLayoutActive();
//                    Utilities.getBatch(currentBatch, new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//                            dragableLayout.doThingsToJson(response);
//                        }
//                    });
                } else
                    setDragableLayoutDeactive();

                if (position % 5 == 4 && currentBatch != 1 && myViewPagerAdapter.storyObjects.size() == position + 1) {
                    myViewPagerAdapter.addStoryObjectFromBatch(currentBatch-1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mustUpdateView = getMustUpdateView();
        container.addView(mustUpdateView);
        mustUpdateView.setVisibility(View.GONE);
        setStatusBarColor();

//        if you want to give offline access , comment the line below
        youShallOrNotPass();


    }


    public FrameLayout getMustUpdateView() {
        FrameLayout containerMustUpdate = new FrameLayout(this);
        containerMustUpdate.setBackgroundColor(Color.WHITE);

        containerMustUpdate.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        TextView textView = new TextView(this);


        textView.setTypeface(Utilities.getFontBold());
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 26);
        textView.setText("You Shall not pass");
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.BLACK);
        FrameLayout.LayoutParams textParams = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        textParams.gravity = Gravity.CENTER;

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (debug)
                    Log.d(TAG, "click on shall not pass");

            }
        });

        containerMustUpdate.addView(textView, textParams);

        return containerMustUpdate;

    }

    public void youShallOrNotPass() {
        if (Utilities.getLastVersionKnownToHumanity() > Utilities.getAppVersion()) {
            if (debug)
                Log.d(TAG, "you shall not pass");
            mustUpdateView.setVisibility(View.VISIBLE);
            shallNotPass = true;
        }
    }


    public void doThingsToJson(JSONObject jsonObject) {
        if (shallNotPass)
            return;
        try {
            final String titleOfDay = jsonObject.getString("titleOfToday");
            final String subTitleOfToday = jsonObject.getString("subTitleOfToday");
            final String detailOfToday = jsonObject.getString("detailOfToday");

            final boolean shouldUpdateViewPaager = (myViewPagerAdapter == null) || (myViewPagerAdapter != null && myViewPagerAdapter.lastIntegeratedBatch != Utilities.getLastBatchNumber());
            if (shouldUpdateViewPaager) myViewPagerAdapter = new MyViewPagerAdapter(this);


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (shallNotPass)
                        return;

                    if (shouldUpdateViewPaager) viewPager.setAdapter(myViewPagerAdapter);
                    if (dragableLayout.lastIntegeratedBatch != Utilities.getLastBatchNumber())
                        dragableLayout.setTexts(titleOfDay, subTitleOfToday, detailOfToday);
                }
            }, 300);


//          if(dragableLayout != null)  dragableLayout.setTexts(titleOfDay , subTitleOfToday , detailOfToday);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        Tracker mTracker = getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    public void setDragableLayoutActive() {


        dragableLayout.setVisibility(View.VISIBLE);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) dragableLayout.getLayoutParams();
        final int firstTopMargin = params.topMargin;
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
//                            Log.d("TAG", "first tp = " + firstTopMargin + " interpolatedTime " + interpolatedTime);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) dragableLayout.getLayoutParams();
                params.topMargin = (int) (-firstTopMargin * interpolatedTime + firstTopMargin);
                dragableLayout.setLayoutParams(params);


            }
        };
        a.setDuration(500);
        dragableLayout.startAnimation(a);

    }

    public void setDragableLayoutDeactive() {
        dragableLayout.setVisibility(View.GONE);

    }

    public void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {

            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.BLACK);
        }

    }


    public Tracker getDefaultTracker() {
        return ((MyApplication) getApplication()).getDefaultTracker();
    }
}
