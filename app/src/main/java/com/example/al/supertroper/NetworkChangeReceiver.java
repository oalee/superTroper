package com.example.al.supertroper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by al on 11/15/15.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    private final String TAG = "NetworkChangeReceiver";
    private final boolean debug = true;

    @Override
    public void onReceive(final Context context, final Intent intent) {


        if (!isOnline(context)) {
            if (debug)
                Log.d(TAG, "is not online");
            return;
        }
        if (debug) {
            Log.d(TAG, "is online");
        }

        Utilities.setContext(context);

        Utilities.checkFromAPI(new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

            }
        }, new Utilities.OnBasicInformationListener() {
            @Override
            public void onBasicInformation(int appVersion, int batch, boolean isNewBatch) {
                if (debug)
                    Log.d(TAG, appVersion + " " + batch + " " + " " + isNewBatch);
            }
        });
    }


    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in air plan mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }
}
