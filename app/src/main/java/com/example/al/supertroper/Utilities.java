package com.example.al.supertroper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by al on 11/9/15.
 */
public class Utilities {
    private static boolean debug = true;
    private static String TAG = "Utilities";
    private static Typeface fontBold;
    private static Typeface fontLight;

    private static Context context;
    private static String SHARED_PREF = "my_shared_prefs";
    public static SharedPreferences sharedPreferences;

    public static String API_URL_BASE = "http://192.168.3.246:54321/";
    public static String API_BASIC_INFROMATION = API_URL_BASE + "api/version";
    public static String API_BATCH_BASE = API_URL_BASE + "api/batch?number=" ;
    public static String BATCH_KEY = "last_batch_number";
    public static String BATCH_JSON_KEY = "json_batch_key";
    public static String LAST_APP_VERSION_KNOWN_KEY = "last_app_version_known";

    public static void checkFromAPI(final Response.Listener<JSONArray> listener, final OnBasicInformationListener onBasicInformationListener) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, API_BASIC_INFROMATION,

                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                if (debug)
                                    Log.d(TAG, response.toString() + " is the response of bi");
                                try {
                                    int batchNumber = Integer.parseInt(response.getString("lastBatchNumber"));
                                    int appVersion = Integer.parseInt(response.getString("appVersion"));
                                    int curBatch = getLastBatchNumber();
                                    sharedPreferences.edit().putInt(LAST_APP_VERSION_KNOWN_KEY, appVersion).commit();
                                    if (onBasicInformationListener != null)
                                        onBasicInformationListener.onBasicInformation(appVersion, batchNumber, curBatch != batchNumber);
                                    getBatch(batchNumber, listener);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (debug)
                            Log.d(TAG, error.toString() + "error in getting bi");
                        getLastBatch(listener);
                    }
                });


        MySingleton.getInstance(context).addToRequestQueue(jsObjRequest);


    }


    public static int getLastVersionKnownToHumanity() {
        return sharedPreferences.getInt(LAST_APP_VERSION_KNOWN_KEY, 1);


    }

    public static void getLastBatch(final Response.Listener<JSONArray> listeners) {
        getBatch(getLastBatchNumber(), listeners);
    }

    public static void getBatch(final int batch, final Response.Listener<JSONArray> listeners) {
//        if (getLastBatchNumber() == batch) {
//            String s = sharedPreferences.getString(BATCH_JSON_KEY + batch, "");
//
//            if (debug)
//                Log.d(TAG, " from sharedPrefrenses" + s);
//            if (s.isEmpty()) {
//                return;
//            }
//            try {
//                listeners.onResponse(new JSONObject(s));
//                return;
//            } catch (JSONException e) {
//                e.printStackTrace();
//                return;
//            }
//        }
//

        String json = sharedPreferences.getString(BATCH_JSON_KEY + batch, "");
        if (!json.isEmpty()) {
            try {
                listeners.onResponse(new JSONArray(json));
                return;
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
        }
        JsonArrayRequest jsObjRequest = new JsonArrayRequest
                (Request.Method.GET, API_BATCH_BASE + batch, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        if (debug)
                            Log.d(TAG, "get batch and its " + response.toString());


                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(BATCH_JSON_KEY + batch, response.toString());
                        if (batch > getLastBatchNumber())
                            editor.putInt(BATCH_KEY, batch);
                        editor.commit();
                        if (listeners != null)
                            listeners.onResponse(response);
                        if (debug)
                            Log.d(TAG, "batch commited ");
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        MySingleton.getInstance(context).addToRequestQueue(jsObjRequest);

        return;
    }

    public static int getLastBatchNumber() {
        return sharedPreferences.getInt(BATCH_KEY, 1);
    }

    public static void setContext(Context context) {
        if (Utilities.context != null)
            if (sharedPreferences != null)
                return;

        Utilities.context = context.getApplicationContext();
        sharedPreferences = context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
    }


    public static int getAppVersion() {


        int versionCode = BuildConfig.VERSION_CODE;

        if (debug)
            Log.d(TAG, "version code is " + versionCode);

        return versionCode;
    }

    public static Typeface getFontLight() {
        if (fontLight == null)
            fontLight = Typeface
                    .createFromAsset(context.getAssets(), "sans_light.ttf");
        return fontLight;
    }

    public static Typeface getFontBold() {

        if (fontBold == null)
            fontBold = Typeface
                    .createFromAsset(context.getAssets(), "sans_bold.ttf");
        return fontBold;
    }

    public interface OnBasicInformationListener {
         void onBasicInformation(int appVersion, int batch, boolean isNewBatch);
    }

}
